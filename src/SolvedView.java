package src;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SolvedView extends JFrame {

	JPanel instancePanel = new JPanel();
	JPanel mainPanel = new JPanel();
	TileButton[][] board;

	SolvedView(Tile[][] solvedTiles) {

		// the board on the left
		instancePanel.setLayout(new GridLayout(Constants.INSTANCE_SIZE, Constants.INSTANCE_SIZE));
		// Boards are always of same width and height
		board = new TileButton[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE];
		for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
			for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
				board[x][y] = new TileButton(x + ", " + y, Constants.noneIcon, x, y);
				Tile t = new Tile(x, y, solvedTiles[x][y].getTopConnections(), solvedTiles[x][y].getRightConnections(),
						solvedTiles[x][y].getBottomConnections(), solvedTiles[x][y].getLeftConnections(),
						solvedTiles[x][y].getTileType());
				board[x][y].setTile(t);
				board[x][y].setSize(Constants.BUTTON_SIZE, Constants.BUTTON_SIZE);
				instancePanel.add(board[x][y]);

				try {
					// should print bars to to the tile, but i think i will include this later.
					File f = new File("ressources" + Constants.sep + t.getTileType() + "Tile.png");
					BufferedImage img = ImageIO.read(f);
					Graphics g = img.getGraphics();

					// If the type is NONE, we want only a grey square with no connections printed.
					if (t.getTileType() != TileType.NONE) {
						g.drawString("" + t.getTopConnections(), 64, 15);
						g.drawString("" + t.getRightConnections(), 118, 70);
						g.drawString("" + t.getBottomConnections(), 64, 118);
						g.drawString("" + t.getLeftConnections(), 10, 70);
					}

					board[x][y].setIcon(new ImageIcon(img));
					board[x][y].setTile(t);

				} catch (Exception ex) {
					System.out.println(ex);
				}
			}
		}

		mainPanel.add(instancePanel);

		this.add(mainPanel);
		this.pack();
		this.setVisible(true);
	}
}
