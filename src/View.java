package src;
// This is the View

// Its only job is to display what the user sees
// It performs no calculations, but instead passes
// information entered by the user to whomever needs
// it. 

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

import src.Controller.MainListener;

public class View extends JFrame {

	public TileButton[][] board;

	// here are the classes, therefore starting with Uppercase Letter
	public JButton TopConnectionsButton = new JButton("Top:0");
	public JButton RightConnectionsButton = new JButton("Right:0");
	public JButton BottomConnectionsButton = new JButton("Bottom:0");
	public JButton LeftConnectionsButton = new JButton("Left:0");
	public JButton TileTypeButton = new JButton("a", Constants.redIcon);

	public JButton SolveButton = new JButton("Solve");
	public JButton Dim4Button = new JButton("4x4");
	public JButton Dim5Button = new JButton("5x5");
	public JButton Dim6Button = new JButton("6x6");

	JPanel mainPanel;
	JPanel instancePanel;
	JPanel buttonAddingPanel;
	JPanel optionPanel;

	View() {
		// Sets up the view and adds the components
		mainPanel = new JPanel();
		instancePanel = new JPanel();
		buttonAddingPanel = new JPanel();
		optionPanel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
		this.setTitle("ConnectMe-Solver");

		// the board on the right
		instancePanel.setLayout(new GridLayout(Constants.INSTANCE_SIZE, Constants.INSTANCE_SIZE));
		// Boards are always of same width and height
		board = new TileButton[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE];
		for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
			for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
				board[x][y] = new TileButton(x + ", " + y, Constants.noneIcon, x, y);
				board[x][y].setTile(new Tile(x, y, 0, 0, 0, 0, TileType.NONE));
				board[x][y].setSize(Constants.BUTTON_SIZE, Constants.BUTTON_SIZE);
				instancePanel.add(board[x][y]);
			}
		}

		// the button adding option on the right
		buttonAddingPanel.setLayout(new java.awt.BorderLayout());
		buttonAddingPanel.add(TopConnectionsButton, BorderLayout.PAGE_START);
		buttonAddingPanel.add(RightConnectionsButton, BorderLayout.LINE_END);
		buttonAddingPanel.add(BottomConnectionsButton, BorderLayout.PAGE_END);
		buttonAddingPanel.add(LeftConnectionsButton, BorderLayout.LINE_START);
		buttonAddingPanel.add(TileTypeButton, BorderLayout.CENTER);

		optionPanel.add(SolveButton);
		optionPanel.add(Dim4Button);
		optionPanel.add(Dim5Button);
		optionPanel.add(Dim6Button);

		// add the panels to the mainPanel

		mainPanel.add(buttonAddingPanel);
		mainPanel.add(optionPanel);
		mainPanel.add(instancePanel);

		this.add(mainPanel);
		this.pack();
		this.setVisible(true);
	}

	void setBoardSize(int size, MainListener mainListener) {

		Constants.INSTANCE_SIZE = size;

		mainPanel.remove(instancePanel);
		instancePanel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
		this.setTitle("ConnectMe-Solver");

		// the board on the left
		instancePanel.setLayout(new GridLayout(Constants.INSTANCE_SIZE, Constants.INSTANCE_SIZE));
		// Boards are always of same width and height
		board = new TileButton[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE];
		for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
			for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
				board[x][y] = new TileButton(x + ", " + y, Constants.noneIcon, x, y);
				board[x][y].setTile(new Tile(x, y, 0, 0, 0, 0, TileType.NONE));
				board[x][y].setSize(Constants.BUTTON_SIZE, Constants.BUTTON_SIZE);
				board[x][y].addActionListener(mainListener);
				instancePanel.add(board[x][y]);
			}
		}

		// add the panels to the mainPanel
		mainPanel.add(instancePanel);

		this.add(mainPanel);
		this.pack();
		this.setVisible(true);

	}

	/**
	 * Connect the JButtons to the ActionListener
	 * 
	 * @param actionListener
	 */
	void addCalculateListener(ActionListener actionListener) {
		TopConnectionsButton.addActionListener(actionListener);
		RightConnectionsButton.addActionListener(actionListener);
		BottomConnectionsButton.addActionListener(actionListener);
		LeftConnectionsButton.addActionListener(actionListener);
		TileTypeButton.addActionListener(actionListener);
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				board[i][j].addActionListener(actionListener);
			}
		}
		SolveButton.addActionListener(actionListener);
		Dim4Button.addActionListener(actionListener);
		Dim5Button.addActionListener(actionListener);
		Dim6Button.addActionListener(actionListener);
	}

	/**
	 * @return TileButton[][] return the fieldArray
	 */
	public TileButton[][] getBoard() {
		return board;
	}

	/**
	 * @param fieldArray the fieldArray to set
	 */
	public void setBoard(TileButton[][] fieldArray) {
		this.board = fieldArray;
	}

}