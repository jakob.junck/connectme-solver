package src;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import src.solver.Solver;

// The Controller coordinates interactions
// between the View and Model

/**
 * Consists of a View and a Model.
 */
public class Controller {

	private View theView;
	private Model theModel;
	private Solver solver;
	static MainListener mainListener;

	public Controller(View theView, Model theModel) {
		this.theView = theView;
		this.theModel = theModel;

		// Tell the View that when ever the calculate button
		// is clicked to execute the actionPerformed method
		// in the CalculateListener inner class
		mainListener = new MainListener();
		this.theView.addCalculateListener(mainListener);
	}

	/**
	 * This is The main listener for all Button actions. Every Event will be handled
	 * by this class.
	 */
	class MainListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Object src = e.getSource();
			if (src == theView.TopConnectionsButton) {
				// top
				theModel.setTopConnections((theModel.getTopConnectionsButton() + 1) % Constants.CONNECTION_COUNT);
				theView.TopConnectionsButton.setText("Top:" + theModel.getTopConnectionsButton());
				theModel.setNewTile(new Tile(theModel.getTopConnections(), theModel.getRightConnections(),
						theModel.getBottomConnections(), theModel.getLeftConnections(), theModel.getTileType()));
			} else if (src == theView.LeftConnectionsButton) {
				// left
				theModel.setLeftConnections((theModel.getLeftConnectionsButton() + 1) % Constants.CONNECTION_COUNT);
				theView.LeftConnectionsButton.setText("Left:" + theModel.getLeftConnectionsButton());
				theModel.setNewTile(new Tile(theModel.getTopConnections(), theModel.getRightConnections(),
						theModel.getBottomConnections(), theModel.getLeftConnections(), theModel.getTileType()));
			} else if (src == theView.RightConnectionsButton) {
				// right
				theModel.setRightConnections((theModel.getRightConnectionsButton() + 1) % Constants.CONNECTION_COUNT);
				theView.RightConnectionsButton.setText("Right:" + theModel.getRightConnectionsButton());
				theModel.setNewTile(new Tile(theModel.getTopConnections(), theModel.getRightConnections(),
						theModel.getBottomConnections(), theModel.getLeftConnections(), theModel.getTileType()));
			} else if (src == theView.BottomConnectionsButton) {
				// bottom
				theModel.setBottomConnections((theModel.getBottomConnectionsButton() + 1) % Constants.CONNECTION_COUNT);
				theView.BottomConnectionsButton.setText("Bottom:" + theModel.getBottomConnectionsButton());
				theModel.setNewTile(new Tile(theModel.getTopConnections(), theModel.getRightConnections(),
						theModel.getBottomConnections(), theModel.getLeftConnections(), theModel.getTileType()));
			} else if (src == theView.TileTypeButton) {
				// Type
				int iconNum = (theModel.getTileType().ordinal() + 1) % Constants.TILE_TYPES;
				theModel.setTileType(iconNum);
				theView.TileTypeButton.setIcon(Constants.icons[iconNum]);
				theModel.setNewTile(new Tile(theModel.getTopConnections(), theModel.getRightConnections(),
						theModel.getBottomConnections(), theModel.getLeftConnections(), theModel.getTileType()));
			} else if (src == theView.Dim4Button) {
				theView.setBoardSize(4, mainListener);
			} else if (src == theView.Dim5Button) {
				theView.setBoardSize(5, mainListener);
			} else if (src == theView.Dim6Button) {
				theView.setBoardSize(6, mainListener);
			} else if (src instanceof TileButton) {
				// Change the icon on the board according to the choosen properties on the right
				Tile t = new Tile(((TileButton) src).getXPos(), ((TileButton) src).getYPos(),
						theModel.getTopConnections(), theModel.getRightConnections(), theModel.getBottomConnections(),
						theModel.getLeftConnections(), theModel.getTileType());

				// If the button is of type NONE, all connections are 0
				if (t.getTileType() == TileType.NONE) {
					t = new Tile(0, 0, 0, 0, TileType.NONE);
				}

				System.out.println("Tile has been added: xPos: " + ((TileButton) src).getXPos() + ", yPos: "
						+ ((TileButton) src).getYPos() + " " + t);

				try {
					// should print bars to to the tile, but i think i will include this later.
					File f = new File("ressources" + Constants.sep + t.getTileType() + "Tile.png");
					BufferedImage img = ImageIO.read(f);
					Graphics g = img.getGraphics();

					// If the type is NONE, we want only a grey square with no connections printed.
					if (t.getTileType() != TileType.NONE) {
						g.drawString("" + t.getTopConnections(), 64, 15);
						g.drawString("" + t.getRightConnections(), 118, 70);
						g.drawString("" + t.getBottomConnections(), 64, 118);
						g.drawString("" + t.getLeftConnections(), 10, 70);
					}

					((JButton) src).setIcon(new ImageIcon(img));
					theView.board[((TileButton) src).getXPos()][((TileButton) src).getYPos()].setTile(t);

				} catch (Exception ex) {
					System.out.println(ex);
				}
				// temporarily using only one constant size, i plan to change this later on
				// (buttons are already added)
			} else if (src == theView.SolveButton) {
				// Start solving
				System.out.println("Solving");
				Tile[][] tiles = new Tile[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE];
				for (int i = 0; i < Constants.INSTANCE_SIZE; i++) {
					for (int j = 0; j < Constants.INSTANCE_SIZE; j++) {
						tiles[i][j] = theView.board[i][j].getTile();
					}
				}
				theModel.setTiles(tiles);

				solver = new Solver(theModel.getTiles());
				if (solver.solve()) {
					System.out.println("A soultion has been found");
					new SolvedView(solver.solutionTiles);
				} else {
					JOptionPane.showMessageDialog(theView,
							"Unable to find a solution. Maybe a misstake in the input?",
							"Error occured",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}