package src.solver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystemException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import java.util.Map.Entry;

import src.*;

public class Solver {
    // contains a String representation of each variable. Is not necessary for
    // solving the instance, but helpful for debugging
    HashMap<String, Integer> varnameToInteger = new HashMap<String, Integer>();

    // Contains non-empty tiles
    ArrayList<Tile> tileList = new ArrayList<>();

    // Contains the clauses which will be needed to generate a file in the DIMACS
    // format
    // For more information checkout
    // http://www.satcompetition.org/2004/format-benchmarks2004.html
    ArrayList<Integer[]> clauses = new ArrayList<>();

    // Contains the solution after the solve() function has been called
    public Tile[][] solutionTiles;

    // model will contain wether the variable is true or not.(Represented by
    // negative or positive value)
    int[] model;

    // The input which we want to solve
    Tile[][] instance;

    public Solver(Tile[][] instance) {
        this.instance = instance;
    }

    public boolean solve() {
        int varCounter = generateVariables(instance);

        // For generating Test-cases:
        try {
            storeTiles(instance,
                    "src" + Constants.sep + "tests" + Constants.sep + "instances_" + Constants.INSTANCE_SIZE
                            + Constants.sep + "Level_xxx.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Tile[][] t =
        // loadTiles("src"+Constants.sep+"tests"+Constants.sep+"instances_"+Constants.INSTANCE_SIZE+Constants.sep+"Level_xxx.ser");

        // Up to this point Variables have been generated, that are needed for every
        // instance of a Constans.INSTANCE_SIZE * Constans.INSTANCE_SIZE board with n
        // Tiles.
        ruleOne();
        ruleTwo();
        ruleThree();
        ruleFour();
        ruleFive();
        ruleSix();

        try {
            generateCnfFile(varCounter, clauses);
        } catch (Exception e) {
            System.out.println(e);
        }

        if (evaluateSat()) {
            printTrueVariables();
            // Everything what comes now is related to set solutionTiles accordingly.
            // This is done by iterating over the model-Array, which encodes which variables
            // are true and which are not.

            // Init solution tiles such that they are non-null
            solutionTiles = new Tile[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE];
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                    solutionTiles[x][y] = new Tile();
                }
            }

            // For each variable. TODO optimize. Involves adding new datastructures
            for (int i = 0; i < model.length; i++) {

                if (i == model.length) {
                    break;
                }

                // Variable is not set to true
                if (model[i] <= 0) {
                    continue;
                }

                // Not the variable we need to build our solution.
                if (getKeyByValue(varnameToInteger, model[i]).startsWith("Square")) {
                    continue;
                }

                if (getKeyByValue(varnameToInteger, model[i]).startsWith("Tile_id:")) {
                    String varname = getKeyByValue(varnameToInteger, model[i]);
                    int l = varname.charAt(varname.length() - 1) - '0';
                    int b = (int) varname.charAt(varname.length() - 5) - '0';
                    int r = (int) varname.charAt(varname.length() - 9) - '0';
                    int t = (int) varname.charAt(varname.length() - 13) - '0';
                    int y = (int) varname.charAt(varname.length() - 17) - '0';
                    int x = (int) varname.charAt(varname.length() - 21) - '0';
                    // This approach only works if we have 10 or less tiles. (Otherwise the id
                    // consist of 2 digits, which is not caught here)
                    int id = (int) varname.charAt(varname.length() - 25) - '0';

                    // Results in the part between "Tile_id:" and "_x:..."
                    // For "Tile_id:13_x:2_y:3..." we get idSting = "13"
                    String idString = varname.split(":")[1].split("_")[0];

                    try {
                        id = Integer.parseInt(idString);
                    } catch (NumberFormatException e) {
                        System.out.println("Could not convert to an integer.");
                    }

                    // initialise it in anyway such that warnings dissappear
                    TileType tileType = TileType.NONE;
                    int temp = 0;
                    for (Tile tile : tileList) {
                        if (temp == id) {
                            tileType = tile.getTileType();
                            break;
                        }
                        temp++;
                    }
                    // We retrieve the variables x,y,t,r,b,l from the variable "Tile_id:...". we got
                    // the tileType via the tileList.
                    // This works since tiles are only added to the list and never rearranged ->
                    // position in tileList determines id
                    solutionTiles[x][y] = new Tile(x, y, t, r, b, l, tileType);

                }
            }

            return true;
        } else {
            return false;
        }

    }

    public static void storeTiles(Tile[][] tiles, String path) throws java.io.IOException {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            outputStream.writeObject(tiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Tile[][] loadTiles(String path) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(path))) {
            return (Tile[][]) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return new Tile[Constants.INSTANCE_SIZE][Constants.INSTANCE_SIZE]; // TODO bad default value. Reason is null
                                                                               // initialisation
        }
    }

    /**
     * Generates variables, which are neededfunction given by tiles. In addition
     * creates Variablenames and puts them into the varnameToInteger map.
     * 
     * @param tiles input which needs to get solved
     * @return number of total variables
     */
    private int generateVariables(Tile[][] tiles) {
        int varCounter = 1;
        // Create the variables for each square
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                for (int i = 0; i < Constants.CONNECTION_COUNT; i++) {
                    // System.out.println("x:"+x+", y:"+y);
                    varnameToInteger.put("Square_x:" + x + "_y:" + y + "_t:" + i, varCounter);
                    varCounter++;
                    varnameToInteger.put("Square_x:" + x + "_y:" + y + "_r:" + i, varCounter);
                    varCounter++;
                    varnameToInteger.put("Square_x:" + x + "_y:" + y + "_b:" + i, varCounter);
                    varCounter++;
                    varnameToInteger.put("Square_x:" + x + "_y:" + y + "_l:" + i, varCounter);
                    varCounter++;

                }
            }
        }

        // Get all tiles that are non-empty and therefore are of any kind of relevance.
        // Get them left to right and then top to bottom
        for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                if (tiles[x][y].getTileType() != TileType.NONE) {
                    tileList.add(new Tile(tiles[x][y]));
                }
            }
        }

        // For each tile create a variable that states that id is on sqaure (x,y)
        for (int id = 0; id < tileList.size(); id++) {
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                    varnameToInteger.put("Square_x:" + x + "_y:" + y + "_id:" + id, varCounter);
                    varCounter++;
                }
            }
        }

        // Create the variables for each tile
        System.out.println("Recognized the following tiles to be non-NONE:");
        int idCounter = 0;
        for (Tile tile : tileList) {
            System.out.println(tile + " with id " + idCounter);
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                    for (int t = 0; t < Constants.CONNECTION_COUNT; t++) {
                        for (int r = 0; r < Constants.CONNECTION_COUNT; r++) {
                            for (int b = 0; b < Constants.CONNECTION_COUNT; b++) {
                                for (int l = 0; l < Constants.CONNECTION_COUNT; l++) {
                                    // This variable is true if tile with id has t connections to the top, r
                                    // connections to the right,
                                    // b to the bottom, l to the left and the tile is at position (x,y)
                                    varnameToInteger.put("Tile_id:" + idCounter + "_x:" + x + "_y:" + y + "_t:" + t
                                            + "_r:" + r + "_b:" + b + "_l:" + l, varCounter);
                                    // System.out.println("Tile_id:" + idCounter + "_x:" + x + "_y:" + y + "_t:" +t+
                                    // "_r:" + r + "_b:" + b + "_l:" + l + " and varCounter=" + varCounter);

                                    varCounter++;
                                }
                            }
                        }
                    }
                }
            }
            idCounter++;
        }

        return varCounter;
    }

    /**
     * Appends a clause to the clauses.
     * 
     * @param intArray the array encoding a clause
     */
    private void addClauses(int[] intArray) {
        Integer[] iaIntegers = new Integer[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            iaIntegers[i] = intArray[i];
        }
        clauses.add(iaIntegers);
    }

    /**
     * Generates a new cnf file in the DIMACS CNF Fromat. This file encodes the
     * problem and is located in the "Solver" folder.
     * Can be solved with any arbitrary SAT-Solver, which can handle DIMACS CNF
     * input.
     * 
     * @param maxVar  amount of occuring variables
     * @param clauses list of Integer[] that enocodes a CNF problem
     * @throws FileSystemException if the creation can not generate a new
     */
    private void generateCnfFile(int maxVar, ArrayList<Integer[]> clauses) throws FileSystemException {

        File file = new File(
                System.getProperty("user.dir") + Constants.sep + "src" + Constants.sep + "currentProblem.cnf");

        // Delete if a file existed previously or if file was not existing
        if (file.delete()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            try {
                file.createNewFile();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        System.out.println("Created empty file:" + file.getPath());

        // Start writing to file
        try {
            try (FileWriter fr = new FileWriter(file, true)) {
                fr.write("c This is a temporary generated file. Do not delete.\nc Time of creation:"
                        + Instant.now().atZone(ZoneId.of("Europe/Berlin")) + "\np cnf " + maxVar + " " + " "
                        + clauses.size() + "\n");

                // Write Clauses
                for (Integer[] clause : clauses) {
                    String s = "";
                    for (int literal : clause) {
                        if (literal == 0) {
                            throw new Exception("0 is not allowed variable");
                        }
                        s = s + " " + literal;

                    }
                    s = s + " 0\n";
                    fr.write(s);
                }

                fr.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * 
     * Add clauses which correspond to the first rule: Every Square has at most one
     * tile. If a variable Tile_id:9_x:2_y:2_t:1_r:2_b:3_l:4 is true, the
     * corresponding Square_... variables have to be true or not.
     */
    private void ruleOne() {
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                // For each square

                // there are tileList.size() many Tiles to consider (= tiles we can move) ->
                // tileList.size() many IDs
                for (int id = 0; id < tileList.size(); id++) {
                    for (int t = 0; t < Constants.CONNECTION_COUNT; t++) {
                        for (int r = 0; r < Constants.CONNECTION_COUNT; r++) {
                            for (int b = 0; b < Constants.CONNECTION_COUNT; b++) {
                                for (int l = 0; l < Constants.CONNECTION_COUNT; l++) {
                                    String varname1 = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:" + t + "_r:"
                                            + r + "_b:" + b + "_l:" + l;
                                    String varname2 = "Square_x:" + x + "_y:" + y + "_id:" + id;

                                    // If we assume that Tile_id:9_x:2_y:2_t:1_r:2_b:3_l:4 is true (meaning tile
                                    // with id 9 is at square (2,2) with corresponding connections),
                                    // then we need Square_x:2_y:2_id:9 to hold.
                                    // a => b is equivalent to (not a) or b
                                    addClauses(new int[] { -varnameToInteger.get(varname1),
                                            varnameToInteger.get(varname2) });

                                }
                            }
                        }
                    }

                    // For each square (x,y) and id, at most one id is at (x,y).
                    // We realise this rule by saying if Square_x:*_y:*_id:i is true,
                    // Square_x:*_y:*_id:j (j != i) is not true. This is a furmula of the form a =>
                    // not b
                    for (int id2 = id + 1; id2 < tileList.size(); id2++) {
                        // This is optimized. As a => (not b) and b => (not a) translate both to (not a)
                        // or (not b), it is sufficient to consider only (id, id2) and not additionally
                        // (id2, id)
                        if (id == id2) {
                            continue;
                        }
                        // a => not b is equivalent to (not a) or (not b)
                        addClauses(new int[] { -varnameToInteger.get("Square_x:" + x + "_y:" + y + "_id:" + id),
                                -varnameToInteger.get("Square_x:" + x + "_y:" + y + "_id:" + id2) });
                    }
                }

            }
        }

        // One id not at multiple squares. Basically use every tile not twice.
        // Square_x:x1_y:y1_id:i => not Square_x:x2_y:y2_id:i iff (x1,y1) != (x2,y2)
        for (int id = 0; id < tileList.size(); id++) {
            for (int x1 = 0; x1 < Constants.INSTANCE_SIZE; x1++) {
                for (int y1 = 0; y1 < Constants.INSTANCE_SIZE; y1++) {
                    for (int x2 = 0; x2 < Constants.INSTANCE_SIZE; x2++) {
                        for (int y2 = 0; y2 < Constants.INSTANCE_SIZE; y2++) {
                            if (x1 == x2 && y1 == y2) {
                                continue;
                            }
                            addClauses(new int[] { -varnameToInteger.get("Square_x:" + x1 + "_y:" + y1 + "_id:" + id),
                                    -varnameToInteger.get("Square_x:" + x2 + "_y:" + y2 + "_id:" + id) });
                        }
                    }

                }
            }
        }

    }

    /**
     * Add clauses which correspond to the second rule: Every tile is at atleast one
     * square.
     * Realised via the clause (Square_x:0_y:0_id:* or Square_x:0_y:1_id:* or ... or
     * Square_x:n_y:n_id:*) for any arbitrary id. Combined with the rule One this
     * enforces that every Tile is at exactly one Square.
     */
    private void ruleTwo() {
        for (int id = 0; id < tileList.size(); id++) {
            ArrayList<Integer> arrayList = new ArrayList<>();
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {

                    arrayList.add(varnameToInteger.get("Square_x:" + x + "_y:" + y + "_id:" + id));

                }
            }
            // Go from ArrayList<Integer> to int[]
            int[] arr = arrayList.stream().mapToInt(i -> i).toArray();
            addClauses(arr);
        }
    }

    /**
     * Add clauses which correspond to the third rule: The neighbouring edges match.
     * Bordering edges (to nowhere) are equal to 0.
     */
    private void ruleThree() {

        /// left connections = right connections
        for (int x = 0; x < Constants.INSTANCE_SIZE - 1; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                for (int i = 0; i < Constants.CONNECTION_COUNT; i++) {
                    String variable1 = "Square_x:" + x + "_y:" + y + "_r:" + i;
                    String variable2 = "Square_x:" + (x + 1) + "_y:" + y + "_l:" + i;
                    // a iff b translates to (not a or b) and (a and not b). This corresponds to two
                    // clauses.
                    addClauses(new int[] { -varnameToInteger.get(variable1), varnameToInteger.get(variable2) });
                    addClauses(new int[] { varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                }
            }
        }

        // top connections = bottom connections. Same logic as above
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE - 1; y++) {
                for (int i = 0; i < Constants.CONNECTION_COUNT; i++) {
                    String variable1 = "Square_x:" + x + "_y:" + y + "_b:" + i;
                    String variable2 = "Square_x:" + x + "_y:" + (y + 1) + "_t:" + i;
                    // a iff b translates to (not a or b) and (a and not b). This corresponds to two
                    // clauses.
                    addClauses(new int[] { -varnameToInteger.get(variable1), varnameToInteger.get(variable2) });
                    addClauses(new int[] { varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                }
            }
        }

        // For Bordering tiles we have to consider "Edge"-Cases :D. Those have to be 0,
        // since there cannot be a tile which matches the connections.
        // Therefore we add fact clauses. Those have to be true.
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                if (y == 0) {
                    // top
                    String variable = "Square_x:" + x + "_y:" + y + "_t:" + 0;
                    addClauses(new int[] { varnameToInteger.get(variable) });
                }
                if (x == Constants.INSTANCE_SIZE - 1) {
                    // right
                    String variable = "Square_x:" + x + "_y:" + y + "_r:" + 0;
                    addClauses(new int[] { varnameToInteger.get(variable) });
                }
                if (y == Constants.INSTANCE_SIZE - 1) {
                    // bottom
                    String variable = "Square_x:" + x + "_y:" + y + "_b:" + 0;
                    addClauses(new int[] { varnameToInteger.get(variable) });
                }
                if (x == 0) {
                    // left
                    String variable = "Square_x:" + x + "_y:" + y + "_l:" + 0;
                    addClauses(new int[] { varnameToInteger.get(variable) });
                }
            }
        }

        // If there is no tile at a square, all connections have to be equal to 0.
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                for (char direction : new char[] { 't', 'r', 'b', 'l' }) {
                    // For each (x,y,direction) tuple the following has to hold:
                    // Either there is a tile or the direction has to be 0.
                    // For any (x,y,dir) tuple:
                    // (Square_x:*_y:*_id:0 or Square_x:*_y:*_id:1 .. or Square_x:*_y:*_id:n) or
                    // Square_x:*_y:*_dir:0
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    for (int id = 0; id < tileList.size(); id++) {
                        arrayList.add(varnameToInteger.get("Square_x:" + x + "_y:" + y + "_id:" + id));
                    }
                    arrayList.add(varnameToInteger.get("Square_x:" + x + "_y:" + y + "_" + direction + ":" + 0));
                    int[] arr = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr);
                }
            }
        }

    }

    /**
     * Add clauses which correspond to the fourth rule: Each square has exactly one
     * amount of connections for a specific direction
     */
    private void ruleFour() {

        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                for (int i = 0; i < Constants.CONNECTION_COUNT; i++) {
                    for (int j = 0; j < Constants.CONNECTION_COUNT; j++) {
                        if (i == j) {
                            continue;
                        }
                        // square at (x,y) has i connections to the top -> square at (x,y) has not j (j
                        // != i) connections to the top
                        // a -> b translates to ((not a) or b). b is here not j connections in the
                        // specific direction
                        String variable1 = "Square_x:" + x + "_y:" + y + "_t:" + i;
                        String variable2 = "Square_x:" + x + "_y:" + y + "_t:" + j;
                        addClauses(new int[] { -varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                        // this is for right
                        variable1 = "Square_x:" + x + "_y:" + y + "_r:" + i;
                        variable2 = "Square_x:" + x + "_y:" + y + "_r:" + j;
                        addClauses(new int[] { -varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                        // this is for bottom
                        variable1 = "Square_x:" + x + "_y:" + y + "_b:" + i;
                        variable2 = "Square_x:" + x + "_y:" + y + "_b:" + j;
                        addClauses(new int[] { -varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                        // this is for left
                        variable1 = "Square_x:" + x + "_y:" + y + "_l:" + i;
                        variable2 = "Square_x:" + x + "_y:" + y + "_l:" + j;
                        addClauses(new int[] { -varnameToInteger.get(variable1), -varnameToInteger.get(variable2) });

                    }
                }
            }
        }
        // At least one amount of connections for every Square in every direction.
        // Combined with the rules above this gives exactly one amount of edges.
        for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
            for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                // For each Square:
                for (char direction : new char[] { 't', 'r', 'b', 'l' }) {
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    for (int i = 0; i < Constants.CONNECTION_COUNT; i++) {
                        arrayList.add(varnameToInteger.get("Square_x:" + x + "_y:" + y + "_" + direction + ":" + i));
                    }
                    // Go from ArrayList<Integer> to int[]
                    int[] arr = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr);
                }

            }
        }

    }

    /**
     * The square variables have to match the Tile variables:
     * If we assume that Tile_id:9_x:2_y:2_t:1_r:2_b:3_l:4 is true (meaning tile
     * with id 9 is at square (2,2) with corresponding connections),
     * then we need Square_x:2_y:2_t:1, Square_x:2_y:2_r:2, Square_x:2_y:2_b:3,
     * Square_x:2_y:2_l:4 to hold as well.
     */
    private void ruleFive() {
        for (int id = 0; id < tileList.size(); id++) {
            for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                    for (int t = 0; t < Constants.CONNECTION_COUNT; t++) {
                        for (int r = 0; r < Constants.CONNECTION_COUNT; r++) {
                            for (int b = 0; b < Constants.CONNECTION_COUNT; b++) {
                                for (int l = 0; l < Constants.CONNECTION_COUNT; l++) {

                                    String tileName = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:" + t + "_r:"
                                            + r + "_b:" + b + "_l:" + l;

                                    String squareT = "Square_x:" + x + "_y:" + y + "_t:" + t;
                                    String squareR = "Square_x:" + x + "_y:" + y + "_r:" + r;
                                    String squareB = "Square_x:" + x + "_y:" + y + "_b:" + b;
                                    String squareL = "Square_x:" + x + "_y:" + y + "_l:" + l;

                                    // TODO this might be removed as it is probably also contained in rule One
                                    String squareId = "Square_x:" + x + "_y:" + y + "_id:" + id;

                                    // if Tile_... then Square_... translates to Tile -> Square translates to (not
                                    // Tile or Square)

                                    addClauses(
                                            new int[] { -varnameToInteger.get(tileName),
                                                    varnameToInteger.get(squareT) });
                                    addClauses(
                                            new int[] { -varnameToInteger.get(tileName),
                                                    varnameToInteger.get(squareR) });
                                    addClauses(
                                            new int[] { -varnameToInteger.get(tileName),
                                                    varnameToInteger.get(squareB) });
                                    addClauses(
                                            new int[] { -varnameToInteger.get(tileName),
                                                    varnameToInteger.get(squareL) });

                                    addClauses(
                                            new int[] { -varnameToInteger.get(tileName),
                                                    varnameToInteger.get(squareId) });

                                }
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * For each tile we add tile specific constraints.
     */
    private void ruleSix() {
        int id = 0;
        for (Tile tile : tileList) {
            switch (tile.getTileType()) {
                // In case it is a red Tile, we only need to add one clause, since we can not
                // rotate red tiles
                case RED:
                    String varString = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + tile.getY() + "_t:"
                            + tile.getTopConnections()
                            + "_r:" + tile.getRightConnections() + "_b:" + tile.getBottomConnections() + "_l:"
                            + tile.getLeftConnections();
                    // System.out.println(varName_to_int.get(varString));
                    addClauses(new int[] { varnameToInteger.get(varString) });
                    break;
                // In case it is a blue tile, we only need to add 4 clauses, since we can onyl
                // rotate blue tiles
                case BLUE:
                    String varString1 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + tile.getY() + "_t:"
                            + tile.getTopConnections()
                            + "_r:" + tile.getRightConnections() + "_b:" + tile.getBottomConnections() + "_l:"
                            + tile.getLeftConnections();
                    String varString2 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + tile.getY() + "_t:"
                            + tile.getLeftConnections()
                            + "_r:" + tile.getTopConnections() + "_b:" + tile.getRightConnections() + "_l:"
                            + tile.getBottomConnections();
                    String varString3 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + tile.getY() + "_t:"
                            + tile.getBottomConnections() + "_r:" + tile.getLeftConnections() + "_b:"
                            + tile.getTopConnections()
                            + "_l:" + tile.getRightConnections();

                    String varString4 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + tile.getY() + "_t:"
                            + tile.getRightConnections() + "_r:" + tile.getBottomConnections() + "_b:"
                            + tile.getLeftConnections()
                            + "_l:" + tile.getTopConnections();
                    addClauses(new int[] { varnameToInteger.get(varString1), varnameToInteger.get(varString2),
                            varnameToInteger.get(varString3), varnameToInteger.get(varString4) });

                    break;
                case ORANGE:
                    ArrayList<Integer> arrayList1 = new ArrayList<>();
                    for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                        for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                            String varString21 = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:"
                                    + tile.getTopConnections()
                                    + "_r:" + tile.getRightConnections() + "_b:" + tile.getBottomConnections() + "_l:"
                                    + tile.getLeftConnections();
                            String varString22 = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:"
                                    + tile.getLeftConnections()
                                    + "_r:" + tile.getTopConnections() + "_b:" + tile.getRightConnections() + "_l:"
                                    + tile.getBottomConnections();
                            String varString23 = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:"
                                    + tile.getBottomConnections() + "_r:" + tile.getLeftConnections() + "_b:"
                                    + tile.getTopConnections()
                                    + "_l:" + tile.getRightConnections();
                            ;
                            String varString24 = "Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:"
                                    + tile.getRightConnections() + "_r:" + tile.getBottomConnections() + "_b:"
                                    + tile.getLeftConnections()
                                    + "_l:" + tile.getTopConnections();

                            arrayList1.add(varnameToInteger.get(varString21));
                            arrayList1.add(varnameToInteger.get(varString22));
                            arrayList1.add(varnameToInteger.get(varString23));
                            arrayList1.add(varnameToInteger.get(varString24));

                        }

                    }
                    int[] arr = arrayList1.stream().mapToInt(i -> i).toArray();
                    addClauses(arr);
                    break;
                // In case it is a green tile, we are only allowed to move this tile. Meaning
                // the orientation of the tile has to stay the same.
                case GREEN:
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                        for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                            arrayList.add(varnameToInteger.get("Tile_id:" + id + "_x:" + x + "_y:" + y + "_t:"
                                    + tile.getTopConnections() + "_r:" + tile.getRightConnections() + "_b:"
                                    + tile.getBottomConnections() + "_l:" + tile.getLeftConnections()));
                        }
                    }
                    // Go from ArrayList<Integer> to int[]
                    int[] arr1 = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr1);
                    break;
                // Purple tiles are fixed in x or y direction, and can not be rotated
                case PURPLERL:
                    arrayList = new ArrayList<>();
                    for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {
                        arrayList.add(varnameToInteger.get("Tile_id:" + id + "_x:" + x + "_y:" + tile.getY() + "_t:"
                                + tile.getTopConnections() + "_r:" + tile.getRightConnections() + "_b:"
                                + tile.getBottomConnections() + "_l:" + tile.getLeftConnections()));
                    }
                    // Go from ArrayList<Integer> to int[]
                    arr1 = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr1);
                    break;
                case PURPLETB:
                    arrayList = new ArrayList<>();
                    for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {
                        arrayList.add(varnameToInteger.get("Tile_id:" + id + "_x:" + tile.getX() + "_y:" + y + "_t:"
                                + tile.getTopConnections() + "_r:" + tile.getRightConnections() + "_b:"
                                + tile.getBottomConnections() + "_l:" + tile.getLeftConnections()));
                    }
                    // Go from ArrayList<Integer> to int[]
                    arr1 = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr1);
                    break;
                // Purple tiles are fixed in x or y direction, and can be rotated
                case BROWNRL:
                    arrayList = new ArrayList<>();
                    for (int x = 0; x < Constants.INSTANCE_SIZE; x++) {

                        String varString11 = "Tile_id:" + id + "_x:" + x + "_y:" + tile.getY() + "_t:"
                                + tile.getTopConnections()
                                + "_r:" + tile.getRightConnections() + "_b:" + tile.getBottomConnections() + "_l:"
                                + tile.getLeftConnections();
                        String varString21 = "Tile_id:" + id + "_x:" + x + "_y:" + tile.getY() + "_t:"
                                + tile.getLeftConnections()
                                + "_r:" + tile.getTopConnections() + "_b:" + tile.getRightConnections() + "_l:"
                                + tile.getBottomConnections();
                        String varString31 = "Tile_id:" + id + "_x:" + x + "_y:" + tile.getY() + "_t:"
                                + tile.getBottomConnections() + "_r:" + tile.getLeftConnections() + "_b:"
                                + tile.getTopConnections()
                                + "_l:" + tile.getRightConnections();

                        String varString41 = "Tile_id:" + id + "_x:" + x + "_y:" + tile.getY() + "_t:"
                                + tile.getRightConnections() + "_r:" + tile.getBottomConnections() + "_b:"
                                + tile.getLeftConnections()
                                + "_l:" + tile.getTopConnections();

                        arrayList.add((varnameToInteger.get(varString11)));
                        arrayList.add((varnameToInteger.get(varString21)));
                        arrayList.add((varnameToInteger.get(varString31)));
                        arrayList.add((varnameToInteger.get(varString41)));
                    }
                    arr1 = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr1);
                    break;
                case BROWNTB:
                    arrayList = new ArrayList<>();
                    for (int y = 0; y < Constants.INSTANCE_SIZE; y++) {

                        String varString11 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + y + "_t:"
                                + tile.getTopConnections()
                                + "_r:" + tile.getRightConnections() + "_b:" + tile.getBottomConnections() + "_l:"
                                + tile.getLeftConnections();
                        String varString21 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + y + "_t:"
                                + tile.getLeftConnections()
                                + "_r:" + tile.getTopConnections() + "_b:" + tile.getRightConnections() + "_l:"
                                + tile.getBottomConnections();
                        String varString31 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + y + "_t:"
                                + tile.getBottomConnections() + "_r:" + tile.getLeftConnections() + "_b:"
                                + tile.getTopConnections()
                                + "_l:" + tile.getRightConnections();

                        String varString41 = "Tile_id:" + id + "_x:" + tile.getX() + "_y:" + y + "_t:"
                                + tile.getRightConnections() + "_r:" + tile.getBottomConnections() + "_b:"
                                + tile.getLeftConnections()
                                + "_l:" + tile.getTopConnections();

                        arrayList.add((varnameToInteger.get(varString11)));
                        arrayList.add((varnameToInteger.get(varString21)));
                        arrayList.add((varnameToInteger.get(varString31)));
                        arrayList.add((varnameToInteger.get(varString41)));
                    }
                    arr1 = arrayList.stream().mapToInt(i -> i).toArray();
                    addClauses(arr1);
                    break;
                default:
                    break;
            }
            id++;
        }
    }

    /*
     * Calls the SAT-Solver which takes as an input the previously generated DIMACS
     * CNF file.
     */
    private boolean evaluateSat() {

        try {
            // TODO not nice, but it works
            Process process = Runtime.getRuntime().exec(
                    "java -jar src/solver/sat4j-sat.jar src/currentProblem.cnf");

            // blocked :(
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            // The solver prints out a v when it found a solution. All following numbers
            // encode valid variable assignment (aka the model). Negative numbers encode
            // that the variable is mapped to false, positive numbers are mapped to true.
            // Example v 1 -2 3:
            // Variable x1 and x3 are true/mapped to 1 and x2 is false/mapped to 0.
            while (((line = reader.readLine()) != null)) {
                // System.out.println(line);
                if (line.charAt(0) == 'v') {
                    // If the interisting part is printed, remove the first two chars from the
                    // String (are always 'v' and ' ')
                    String[] sA = line.substring(2).split(" ");
                    model = new int[sA.length];
                    int temp = 0;
                    for (String s : sA) {
                        model[temp] = Integer.parseInt(s);
                        temp++;
                    }
                }
            }
            // Potential problem, as our calling process has to wait for the jar-process to
            // finish.
            process.waitFor();
            // int exitCode = process.waitFor();
            // System.out.println("\nExited with error code : " + exitCode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // The sat solver can not find a model. Most likely a wrong input.
        if (model == null) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Iterates over the model retrieved by the solver and prints only true
     * variables.
     */
    private void printTrueVariables() {
        System.out.println("Printing ture variables:");
        for (int i : model) {
            if (i > 0) {
                System.out.println(Integer.toString(i) + " " + getKeyByValue(varnameToInteger, i));
            }

        }
    }

    private <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

}
