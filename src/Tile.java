package src;

import java.io.Serializable;

/**
 * A Tile consists of up to 4 connections in each directionand a position on the
 * board.
 * It additionally has a TileType/Colour.
 */
public class Tile implements Serializable {
    private int topConnections;
    private int rightConnections;
    private int bottomConnections;
    private int leftConnections;
    private int x;
    private int y;
    private TileType type;

    public Tile() {
        this(0, 0, 0, 0, TileType.NONE);
    }

    public Tile(int topConnections, int rightConnections, int bottomConnections, int leftConnections, TileType type) {
        this.topConnections = topConnections;
        this.rightConnections = rightConnections;
        this.bottomConnections = bottomConnections;
        this.leftConnections = leftConnections;
        this.type = type;
    }

    public Tile(int x, int y, int topConnections, int rightConnections, int bottomConnections, int leftConnections,
            TileType type) {
        this.topConnections = topConnections;
        this.rightConnections = rightConnections;
        this.bottomConnections = bottomConnections;
        this.leftConnections = leftConnections;
        this.type = type;
        this.x = x;
        this.y = y;
    }

    public Tile(Tile tile) {
        this.topConnections = tile.topConnections;
        this.rightConnections = tile.rightConnections;
        this.bottomConnections = tile.bottomConnections;
        this.leftConnections = tile.leftConnections;
        this.type = tile.type;
        this.x = tile.x;
        this.y = tile.y;
    }

    public String toString() {
        return "top: " + topConnections + ", right: " + rightConnections + ", bottom:" + bottomConnections + ", left:"
                + leftConnections + ", colour:" + type + ", x:" + x + ", y:" + y;
    }

    public int getTopConnections() {
        return topConnections;
    }

    public int getRightConnections() {
        return rightConnections;
    }

    public int getBottomConnections() {
        return bottomConnections;
    }

    public int getLeftConnections() {
        return leftConnections;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public TileType getTileType() {
        return type;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
