package src.tests;

import org.junit.Test;

import src.Constants;
import src.Tile;
import src.solver.Solver;

public class Tests {
    /*
     * If this test fails, consider adding ,
     * "java.test.config": {
     * "name": "myTestConfig",
     * "workingDirectory": "${workspaceFolder}"
     * to your settings.json.
     * This selects the current working directory.
     * }
     */

    @Test()
    public void check4x4() {

        // Red and Green Tiles
        checkInstance(4, "001", true);
        // Connection count up to 4
        checkInstance(4, "004", true);
        // Blue Tile
        checkInstance(4, "007", true);
        // Orange Tile
        checkInstance(4, "020", true);
        // More than 10 tiles
        checkInstance(4, "179", true);
        // Purple Tiles (RL and TB)
        checkInstance(4, "i00", true);
        // purple where it does not work
        checkInstance(4, "-00", false);
        // Brown Tiles (RL and TB)
        checkInstance(4, "i01", true);

    }

    @Test
    public void check5x5() {
        // Purple Tile RL
        checkInstance(5, "001", true);
        checkInstance(5, "054", true);
    }

    @Test
    public void check6x6() {
        // i is code for imaginary, meaning it is not a instance by the game
        checkInstance(6, "i00", true);

        // Takes approximately 17.4 sec on my machine, be careful
        checkInstance(6, "001", true);

    }

    @Test
    public void test() {
        int a = 2;
        assert (1 == 3 - a);
    }

    private void checkInstance(int size, String level, boolean goal) {
        Tile[][] tiles = Solver.loadTiles("src" + Constants.sep + "tests" + Constants.sep + "instances_" + size
                + Constants.sep + "Level_" + level + ".ser");
        System.out.println(tiles.length);
        Constants.INSTANCE_SIZE = tiles.length;
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles.length; j++) {
                System.out.println(tiles[i][j]);
            }
        }
        Solver s = new Solver(tiles);
        boolean b = s.solve();
        assert (b == goal);
    }

}