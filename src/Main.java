package src;

/**
 * Run this main() to start the program.
 */
public class Main {

    public static void main(String[] args) {

        View theView = new View();

        Model theModel = new Model();

        new Controller(theView, theModel);

        theView.setVisible(true);

    }
}