package src;

import java.util.Objects;

/**
 * The Model performs all the calculations needed and that is it.
 */
public class Model {

    private Tile[][] tiles;

    // here are the attributes necessary for adding new tiles, therefore starting
    // with Lowercase Letters
    private int topConnections = 0;
    private int rightConnections = 0;
    private int bottomConnections = 0;
    private int leftConnections = 0;
    private TileType tileType = TileType.RED;
    private Tile newTile = new Tile(topConnections, rightConnections, bottomConnections, leftConnections, tileType);

    public Model() {
    }

    public Model(Tile[][] tiles, int topConnections, int rightConnections, int bottomConnections, int leftConnections,
            TileType tileType, Tile tile) {
        this.tiles = tiles;
        this.topConnections = topConnections;
        this.rightConnections = rightConnections;
        this.bottomConnections = bottomConnections;
        this.leftConnections = leftConnections;
        this.tileType = tileType;
        this.newTile = tile;
    }

    public Tile[][] getTiles() {
        return this.tiles;
    }

    public void setTiles(Tile[][] tiles) {
        this.tiles = tiles;
    }

    public Model tiles(Tile[][] tiles) {
        setTiles(tiles);
        return this;
    }

    public Model topConnections(int topConnections) {
        setTopConnections(topConnections);
        return this;
    }

    public Model rightConnections(int rightConnections) {
        setRightConnections(rightConnections);
        return this;
    }

    public Model bottomConnections(int bottomConnections) {
        setBottomConnections(bottomConnections);
        return this;
    }

    public Model leftConnections(int leftConnections) {
        setLeftConnections(leftConnections);
        return this;
    }

    public Model tileType(TileType tileType) {
        setTileType(tileType);
        return this;
    }

    public Model tile(Tile tile) {
        setNewTile(tile);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Model)) {
            return false;
        }
        Model model = (Model) o;
        return Objects.equals(tiles, model.tiles) && topConnections == model.topConnections
                && rightConnections == model.rightConnections && bottomConnections == model.bottomConnections
                && leftConnections == model.leftConnections && Objects.equals(tileType, model.tileType)
                && Objects.equals(newTile, model.newTile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tiles, topConnections, rightConnections, bottomConnections, leftConnections, tileType,
                newTile);
    }

    @Override
    public String toString() {
        return "{" +
                " tiles='" + getTiles() + "'" +
                ", topConnections='" + getTopConnections() + "'" +
                ", rightConnections='" + getRightConnections() + "'" +
                ", bottomConnections='" + getBottomConnections() + "'" +
                ", leftConnections='" + getLeftConnections() + "'" +
                ", tileType='" + getTileType() + "'" +
                "}";
    }

    /**
     * @return int return the topConnections
     */
    public int getTopConnectionsButton() {
        return topConnections;
    }

    /**
     * @param topConnections the topConnections to set
     */
    public void setTopConnections(int topConnections) {
        this.topConnections = topConnections;
    }

    /**
     * @return int return the rightConnections
     */
    public int getRightConnectionsButton() {
        return rightConnections;
    }

    /**
     * @param rightConnections the rightConnections to set
     */
    public void setRightConnections(int rightConnections) {
        this.rightConnections = rightConnections;
    }

    /**
     * @return int return the bottomConnections
     */
    public int getBottomConnectionsButton() {
        return bottomConnections;
    }

    /**
     * @param bottomConnections the bottomConnections to set
     */
    public void setBottomConnections(int bottomConnections) {
        this.bottomConnections = bottomConnections;
    }

    /**
     * @return int return the leftConnections
     */
    public int getLeftConnectionsButton() {
        return leftConnections;
    }

    /**
     * @param leftConnections the leftConnections to set
     */
    public void setLeftConnections(int leftConnections) {
        this.leftConnections = leftConnections;
    }

    /**
     * @return TileType return the tileType
     */
    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
        // System.out.println("TileType: "+this.tileType.ordinal());
    }

    public void setTileType(int ordinal) {
        this.tileType = TileType.values()[ordinal];
        // System.out.println("TileType: "+this.tileType.ordinal());
    }

    public void setNewTile(Tile t) {
        this.newTile = t;
    }

    public int getTopConnections() {
        return topConnections;
    }

    public int getRightConnections() {
        return rightConnections;
    }

    public int getBottomConnections() {
        return bottomConnections;
    }

    public int getLeftConnections() {
        return leftConnections;
    }

}
