package src;

import java.nio.file.FileSystems;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * This class contains all magic numbers and links to
 */
public final class Constants {

    // .getSeparator() is a way to make this project "future proof" and applicable
    // for unix and windows.
    public static final String sep = FileSystems.getDefault().getSeparator();

    public static final int WINDOW_WIDTH = 500;
    public static final int WINDOW_HEIGHT = 400;

    // I consider removing this as this can be retrieved via model.size
    public static int INSTANCE_SIZE = 4;
    public static final int DIRECTION_COUNT = 4;

    // 5 possible connections in each direction, somewhere in [0,4]
    public static final int CONNECTION_COUNT = 5;
    public static final int BUTTON_SIZE = 60;

    // For Buttons
    public static final Icon redIcon = new ImageIcon("ressources" + sep + "REDTile.png");
    public static final Icon blueIcon = new ImageIcon("ressources" + sep + "BLUETile.png");
    public static final Icon orangeIcon = new ImageIcon("ressources" + sep + "ORANGETile.png");
    public static final Icon greenIcon = new ImageIcon("ressources" + sep + "GREENTile.png");
    public static final Icon purplerlIcon = new ImageIcon("ressources" + sep + "PURPLERLTile.png");
    public static final Icon purpletbIcon = new ImageIcon("ressources" + sep + "PURPLETBTile.png");
    public static final Icon brownrlIcon = new ImageIcon("ressources" + sep + "BROWNRLTile.png");
    public static final Icon browntbIcon = new ImageIcon("ressources" + sep + "BROWNTBTile.png");
    public static final Icon noneIcon = new ImageIcon("ressources" + sep + "NONETile.png");
    public static final int TILE_TYPES = 9;

    public static final Icon[] icons = { redIcon, blueIcon, orangeIcon, greenIcon, purplerlIcon, purpletbIcon,
            brownrlIcon, browntbIcon, noneIcon };

    /**
     * An private constructor such that this class can not be instanciated.
     */
    private Constants() {
    }
}