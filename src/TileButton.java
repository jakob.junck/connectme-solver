package src;

import javax.swing.Icon;
import javax.swing.JButton;

/**
 * A button which additionally stores the
 * x and y coordinates of the square where it is located.
 */
public class TileButton extends JButton {
    private int xPos;
    private int yPos;
    public Tile tile;

    TileButton(String s, Icon i, int x, int y) {
        // Swap this to super (s, i); for showing coordinates
        super(i);
        this.xPos = x;
        this.yPos = y;
    }

    /**
     * @return int return the xPos
     */
    public int getXPos() {
        return xPos;
    }

    /**
     * @param xPos the xPos to set
     */
    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * @return int return the yPos
     */
    public int getYPos() {
        return yPos;
    }

    /**
     * @param yPos the yPos to set
     */
    public void setYPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * @return Tile return the tile
     */
    public Tile getTile() {
        return tile;
    }

    /**
     * @param tile the tile to set
     */
    public void setTile(Tile tile) {
        this.tile = tile;
    }

}
