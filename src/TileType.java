package src;

public enum TileType {
    RED,
    BLUE,
    ORANGE,
    GREEN,
    PURPLERL,
    PURPLETB,
    BROWNRL,
    BROWNTB,
    NONE
}