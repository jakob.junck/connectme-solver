# ConnectMe-Solver

## Description

If you rather watch then read, I explain this project here:

[![Watch the video](https://fiverr-res.cloudinary.com/t_mobile_web_2,q_auto,f_auto/gigs/305744119/original/91036417e372bd19b58c646f54532c98cc726326.png)](https://youtu.be/algKXHgLQIU)


This Project is a solver for the Connect Me - Logic Puzzle game, which is a mobile game downloadable at the [Google Playstore](https://play.google.com/store/apps/details?id=net.bohush.connect.me.logic.puzzle). I believe it is not necessary to download the game to understand the rules of the game and how this solver works. (Although, it may deepen the understanding.) The following explanation below together with the provided images should be sufficient to understand how the game works:

Each puzzle consists of a board of the size between 4x4 and 6x6. There are multiple *Tiles* in different colours on the board. Each *Tile* is on exactly one *Square*. Each tile has up to 4 different *connections* in each direction (top, right, bottom, left). Each tile has a colour that specifies a *Type* and assigns different properties. In general, tiles can be moved (to a different square) and turned (on the same square). 

The four basic colours that are necessary to understand the example below are the following ones:

Red: The tile can not be moved nor turned. \
Green: The tile can be moved, but not turned.\
Blue: The tile can be turned, but not moved.\
Orange: The tile can be moved and turned.

There are more complex tiles, that are also in the game:

Purple: The tile can be moved only in vertical or horizontal direction, but not turned.\
Brown: The tile can  be moved only in vertical or horizontal direction and also turned.

The aim of the player is to solve the instance such that the following condition is satisfied:
- Each tile _a_ has to be turned in such a way that there are exactly as many connections pointing to tile _b_ as there are pointing from _b_ to _a_. An example of a solved instance can be seen below. 

- There is only one strongly connected component, e.g. there are not two separated groups of tiles. If the tiles are nodes within a graph, connections between tiles represent edges between nodes, it should be possible to reach any node form any node. 

  - **This constraint is not implemented yet.** However, for any example from the game, it returned a valid solution.

In order to achieve this goal, the player moves and turns tiles across the board. The following table explains this visually for an instance of size 4x4 with 8 tiles:

<table>
  <caption>Example How an instance is solved</caption>

  <tr>
    <th>Image of Instance</th>
    <th>Description</th>
  </tr>
  <tr>
    <td><img src="documentation/Screenshot1.jpg" alt="drawing" width="200" title= "An instance in it starting position"></td>
    <td>An instance in it starting position</td>
  </tr>
  <tr>
    <td><img src="documentation/Screenshot2.jpg" alt="drawing" width="200" title= "This puzzle is nearly solved"></td>
    <td>This puzzle is nearly solved</td>
  </tr>
  <tr>
    <td><img src="documentation/Screenshot3.jpg" alt="drawing" width="200" title= "Solved instance"></td>
    <td>Solved instance</td>
  </tr>
</table>

## Usage
This solver is meant to be used once a player is not able to solve a level on its own. The user is able to specify the size of the board via the corresponding buttons 4x4, 5x5 and 6x6. On the left, the user can specify a colour of the tile by clicking repeatedly the button in the middle. The connections are entered in the same way by clicking on the surrounding buttons. After the correct properties (amount of connections in each direction and tile colour) of the buttons are entered, the user can press a square on the board and thereby adding the button to the field. Once the user created a valid instance (e.g. one that can be solved) and clicks the button _Solve_ a solution will be calculated by the solver. If an instance is unsolveable, the program will inform the user. The solution will be displayed in a new panel and usually takes less than 1, 3 and 15 seconds to be solved. The time necessary to solve is dependent on the boardsize, the amount of the tiles and the type of the tiles.

The solver is tested for JDK 11. (Should work for higher versions as well.)

The user can start the solver by clicking on the .jar-file or via 
```
java -jar --enable-preview connectme-solver.jar
```
you might not need (--enable-preview) or additionally for linux users via 

```
chmod +x start-CMS.sh
./start-CMS.sh
```
that simply runs the command from above.
## TODOs
- Adding a build tool. Currently using the default tool provided by vscode.
- Removing files after they have been created.
## About
The project originated during my holidays, where I wanted to try out to work on a more complex software project for the second time. 
